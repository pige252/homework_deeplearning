import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/",one_hot=True)

x = tf.placeholder(tf.float32,[None,784])
#W = tf.Variable(tf.zeros([784,10]))
#b = tf.Variable(tf.zeros([10]))
#y = tf.nn.softmax(tf.matmul(x, W) + b)
W = tf.Variable(tf.zeros([784,300]))
b = tf.Variable(tf.zeros([300]))
x2 = tf.nn.softmax(tf.matmul(x,W)+b)

W2 = tf.Variable(tf.zeros([300,10]))
b2 = tf.Variable(tf.zeros([10]))
y = tf.nn.softmax(tf.matmul(x2,W2)+b2)

ans = tf.placeholder(tf.float32,[None,10])
loss = tf.reduce_mean(-tf.reduce_sum(ans * tf.log(y),1))
opt = tf.train.GradientDescentOptimizer(0.02).minimize(loss)

sess = tf.Session()
sess.run(tf.global_variables_initializer())

def test():
    x_train = mnist.test.images[0:1]
    answer = sess.run(y,feed_dict={x:x_train})
    print('\ny vector is', answer)
    print('my guess is', answer.argmax())

train_tot = 10000
batch_size = 100

test()
for i in range(train_tot):
    batch_xs, batch_ys = mnist.train.next_batch(batch_size)
    error, _ = sess.run([loss,opt],feed_dict={x:batch_xs,ans:batch_ys})
    if i % 500 == 0:
        print('batch',i,'error = %.3f' %error)

test()

correct = tf.equal(tf.argmax(y,1),tf.argmax(ans,1))
accuracy = tf.reduce_mean(tf.cast(correct,tf.float32))
images = mnist.test.images
labels = mnist.test.labels
print('\nmodel accuracy:',sess.run(accuracy,feed_dict={x:images,ans:labels}))

print(sess.run(W[0:784,0]))



#####################################################################
#single layer
#####################################################################

W3 = tf.Variable(tf.zeros([784,10]))
b3 = tf.Variable(tf.zeros([10]))
y2 = tf.nn.softmax(tf.matmul(x, W3) + b3)

ans2 = tf.placeholder(tf.float32,[None,10])
loss2 = tf.reduce_mean(-tf.reduce_sum(ans2 * tf.log(y2),1))
opt2 = tf.train.GradientDescentOptimizer(0.02).minimize(loss2)

sess = tf.Session()
sess.run(tf.global_variables_initializer())

def test2():
    x_train = mnist.test.images[0:1]
    answer = sess.run(y2,feed_dict={x:x_train})
    print('\ny vector is', answer)
    print('my guess is', answer.argmax())

print('\n\n\n\n\n\n\n\nsoftmax_singlayer')
test2()
for i in range(train_tot):
    batch_xs, batch_ys = mnist.train.next_batch(batch_size)
    error, _ = sess.run([loss2,opt2],feed_dict={x:batch_xs,ans2:batch_ys})
    if i % 500 == 0:
        print('batch',i,'error = %.3f' %error)

test2()

correct = tf.equal(tf.argmax(y2,1),tf.argmax(ans2,1))
accuracy = tf.reduce_mean(tf.cast(correct,tf.float32))
images = mnist.test.images
labels = mnist.test.labels
print('\nmodel accuracy:',sess.run(accuracy,feed_dict={x:images,ans2:labels}))

print(sess.run(W3[0:784,0]))