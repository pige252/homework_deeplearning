import tensorflow as tf
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

T,F = 1. , -1.
bias = 1.

#train_in = [[T,T,bias],[T,F,bias],[F,T,bias],[F,F,bias]]
#train_out = [[T],[F],[F],[F]]
x = tf.placeholder(tf.float32)
y = tf.placeholder(tf.float32)

x_tr = [[T,T,bias],[T,F,bias],[F,T,bias],[F,F,bias]]
y_tr = [[T],[F],[F],[F]]

w = tf.Variable(tf.random_normal([3,1]))

def step(x):
    is_greater = tf.greater(x,0)
    as_float = tf.to_float(is_greater)
    doubled = tf.multiply(as_float,2)
    return tf.subtract(doubled,1)

output = step(tf.matmul(x,w))
error = tf.subtract(y,output)
mse = tf.reduce_mean(tf.square(error))

#delta = tf.matmul(train_in,error,transpose_a=True)
#train = tf.assign(w,tf.add(w,delta))
delta = tf.matmul(x,error,transpose_a=True)
train = tf.assign(w,tf.add(w,delta))

sess = tf.Session()
sess.run(tf.global_variables_initializer())
err, target = 1,0
epoch , max_epoch = 0,1000

def test():
    print('\nweights/bias\n',sess.run(w,feed_dict={x:x_tr,y:y_tr}))
    print('output\n',sess.run(output,feed_dict={x:x_tr,y:y_tr}))
    print('mse: ',sess.run(mse,feed_dict={x:x_tr,y:y_tr}),'\n')

test()
while err > target and epoch < max_epoch:
    epoch += 1
    err,_ = sess.run([mse,train],feed_dict={x:x_tr,y:y_tr})
    print('epoch: ',epoch, 'mse: ',err)
test()
